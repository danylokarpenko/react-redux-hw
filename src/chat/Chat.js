import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Spinner from './Spinner';
import Header from './Header';
import MessageList from '../messages/MessageList';
import MessageInput from '../messages/MessageInput';
import PageHeader from './PageHeader';
import PageFooter from './PageFooter';
import EditModal from './EditModal';
import '../App.css';
import callWebApi from '../helpers/webApiHelper';
import { setMessages } from '../messages/actions';
import * as actions from './actions';

class Chat extends Component {
    componentDidMount() {
        this.fetchData();
    }

    async fetchData() {
        const response = await callWebApi({
            endpoint: 'https://edikdolynskyi.github.io/react_sources/messages.json',
            type: 'GET'
        });
        const messages = response.map(message => Object.assign(message, { reactions: [] }))
        // get random user 
        const { userId, avatar, user } = messages[0];

        this.props.setMessages(messages);
        this.props.setUser({
            userId,
            avatar,
            user
        });
        this.props.setIsLoading(false);
    }
    render() {
        const { isLoading, editMessage } = this.props;
        return (
            isLoading ? <Spinner /> :
                <div className="Page-container">
                    <PageHeader />
                    <div className="Chat">
                    {editMessage && <EditModal />}
                        <Header />
                        <MessageList />
                        <MessageInput />
                    </div>
                    <PageFooter />
                </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isLoading: state.chat.isLoading,
        editMessage: state.chat.editMessage
    }
}

const mapDispatchToProps = {
    ...actions,
    setMessages
}

Chat.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    editMessage: PropTypes.object,
    setMessages: PropTypes.func.isRequired,
    setUser: PropTypes.func.isRequired,
    setIsLoading: PropTypes.func.isRequired
}

Chat.defaultProps = {
    editMessage: undefined
  };

export default connect(mapStateToProps, mapDispatchToProps)(Chat);