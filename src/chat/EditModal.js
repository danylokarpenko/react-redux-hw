import React, { Component } from 'react';
import { setEditMessage } from './actions';
import { updateMessage } from '../messages/actions';
import { connect } from 'react-redux';

class EditModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            textInput: this.props.message.text
        }
    }
    onChange(e, keyword) {
        const value = e.target.value;
        this.setState({
            [keyword]: value
        })
    }
    formMessageData() {
        const text = this.state.textInput;
        return {
            text,
            editedAt: new Date().toISOString()
        }
    }
    onUpdate() {
        const { id } = this.props.message;
        const messageData = this.formMessageData();

        this.props.updateMessage(id, messageData)
        this.props.setEditMessage(null);
        this.setState({
            textInput: ''
        })
    }
    onClose() {
        this.props.setEditMessage(null);
        this.setState({
            textInput: ''
        })
    }
    render() {
        return (
            <div className="modal" style={{ display: "block" }} tabIndex="-1" role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Edit Message</h5>
                            <button onClick={() => this.onClose()} type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p>Modal body text goes here.</p>
                            <div className="MessageInput">
                                <div className="input-group">
                                    <textarea placeholder="Type here.." className="form-control" aria-label="With textarea" value={this.state.textInput} onChange={(e) => this.onChange(e, 'textInput')}></textarea>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button onClick={() => this.onUpdate()} type="button" className="btn btn-primary">Save changes</button>
                            <button onClick={() => this.onClose()} type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        message: state.chat.editMessage || {}
    }
}

const mapDispatchToProps = {
    setEditMessage,
    updateMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(EditModal);