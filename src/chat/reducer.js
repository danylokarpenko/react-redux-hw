import { SET_IS_LOADING, SET_USER, SET_EDIT_MESSAGE } from './actionTypes';

const initialState = {
    isLoading: true,
    user: {},
    name: 'My Chat',
    editMessage: null
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_IS_LOADING: {
            const { isLoading } = action.payload;
            return { ...state, isLoading };
        }

        case SET_USER: {
            const { user } = action.payload;
            return { ...state, user };
        }

        case SET_EDIT_MESSAGE: {
            const { message: editMessage } = action.payload;
            return { ...state, editMessage };
        }

        default:
            return state;
    }
}