import { SET_IS_LOADING, SET_USER, SET_EDIT_MESSAGE } from './actionTypes';

export const setIsLoading = (isLoading) => ({
    type: SET_IS_LOADING,
    payload: {
        isLoading
    }
});

export const setUser = (user) => ({
    type: SET_USER,
    payload: {
        user
    }
});

export const setEditMessage = (message) => ({
    type: SET_EDIT_MESSAGE,
    payload: {
        message
    }
});
