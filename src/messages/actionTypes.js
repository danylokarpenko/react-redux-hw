export const ADD_MESSAGE = 'ADD_MESSAGE';
export const UPDATE_MESSAGE = 'UPDATE_MESSAGE';
export const DELETE_MESSAGE = 'DELETE_MESSAGE';
export const SET_MESSAGES = 'SET_MESSAGES';
export const SET_LIKE = 'SET_LIKE';
