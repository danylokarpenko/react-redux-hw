import { ADD_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE, SET_MESSAGES, SET_LIKE } from './actionTypes';
import service from './service';

export const addMessage = (data) => ({
    type: ADD_MESSAGE,
    payload: {
        id: service.getNewId(),
        data
    }
});

export const updateMessage = (id, data) => ({
    type: UPDATE_MESSAGE,
    payload: {
        id,
        data
    }
});

export const deleteMessage = (id) => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});

export const setMessages = (messages) => ({
    type: SET_MESSAGES,
    payload: {
        messages
    }
});

export const setLike = (id, userId) => ({
    type: SET_LIKE,
    payload: {
        id,
        userId
    }
});
