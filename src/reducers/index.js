import { combineReducers } from 'redux';
import messages from '../messages/reducer';
import chat from '../chat/reducer';

const rootReducer = combineReducers({
    messages,
    chat
})

export default rootReducer;